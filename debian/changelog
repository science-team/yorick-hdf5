yorick-hdf5 (0.8.0-9) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.

  [ Thibaut Paumard ]
  * Update VCS URIs.
  * Bump Policy to 4.7.0.
  * Bug fix: "ftbfs with GCC-14", thanks to Matthias Klose (Closes:
    #1075701).

 -- Thibaut Paumard <thibaut@debian.org>  Tue, 30 Jul 2024 11:37:14 +0200

yorick-hdf5 (0.8.0-8) unstable; urgency=high

  * Reupload to unstable.

 -- Thibaut Paumard <thibaut@debian.org>  Fri, 02 Dec 2016 16:03:21 +0100

yorick-hdf5 (0.8.0-7) experimental; urgency=medium

  * Bug fix: "FTBFS against HDF5 v1.10", thanks to Gilles Filippini
    (Closes: #842817).
  * Check against Policy 3.9.8.

 -- Thibaut Paumard <thibaut@debian.org>  Wed, 02 Nov 2016 11:43:31 +0100

yorick-hdf5 (0.8.0-6) experimental; urgency=high

  * Make yorick-hdf5 compile with HDF5 v1.10. The package is still broken
    on 32-bit arches, although it builds fine, so not closing the bug
    (#842817).

 -- Thibaut Paumard <thibaut@debian.org>  Wed, 02 Nov 2016 11:29:21 +0100

yorick-hdf5 (0.8.0-5) unstable; urgency=medium

  * Team upload.
  * Support hdf5 1.8.13 new packaging layout (closes: #756703).

 -- Gilles Filippini <pini@debian.org>  Sun, 03 Aug 2014 12:40:36 +0200

yorick-hdf5 (0.8.0-4) unstable; urgency=low

  * Fortify (don't rely on yorick to provide right flags)

 -- Thibaut Paumard <paumard@users.sourceforge.net>  Thu, 28 Jun 2012 17:39:22 +0200

yorick-hdf5 (0.8.0-3) unstable; urgency=low

  * Amend debian/control to abide by Debian Science Policy
  * Patch Makefile instead of running yorick -batch make.i
  * Simplfiy debian/rules with short dh notation
  * Deactivate pre-0.6.2 bug warning, its really old.

 -- Thibaut Paumard <paumard@users.sourceforge.net>  Wed, 27 Jun 2012 11:26:58 +0200

yorick-hdf5 (0.8.0-2) unstable; urgency=low

  * Change HDF5 build dep.
  * Use "3.0 (quilt)" format.
  * Bump standards version.

 -- Thibaut Paumard <paumard@users.sourceforge.net>  Mon, 23 Jan 2012 22:33:21 +0100

yorick-hdf5 (0.8.0-1) unstable; urgency=low

  * New upstream release (Closes: #556923)
  * Use quilt for patch management
  * Checked against policy 3.8.3

 -- Thibaut Paumard <paumard@users.sourceforge.net>  Wed, 18 Nov 2009 13:02:06 +0100

yorick-hdf5 (0.6.1-2) unstable; urgency=low

  * Bug fix: "yorick-hdf5: FTBFS: /usr/bin/make: invalid option -- 2",
    thanks to Lucas Nussbaum (Closes: #476034). CFLAGS was passed as
    argument to make in debian/rules in a way that just breaks.

 -- Thibaut Paumard <paumard@users.sourceforge.net>  Tue, 15 Apr 2008 17:23:59 +0200

yorick-hdf5 (0.6.1-1) unstable; urgency=low

  * New upstream release
  * debian/watch: file added
  * debian/rules: converted to dh_installyorick
     * added debian/ynstall
     * added debian/yorick-hdf5.packinfo
     * added debian/yorick-hdf5.keywords
  * debian/control:
     * DM-Upload-Allowed field added
     * upgraded to Standards-Version: 3.7.3.0
     * upgrade build dependency on yorick-dev
       (>= 2.1.05+dfsg-2~bpo40+1)
  * debian/copyright: updated and converted to the machine-readable format
    proposed at http://wiki.debian.org/Proposals/CopyrightFormat

 -- Thibaut Paumard <paumard@users.sourceforge.net>  Tue, 15 Jan 2008 11:05:31 +0100

yorick-hdf5 (0.6-1) unstable; urgency=low

  * Initial Release. Closes: #366705

 -- Thibaut Paumard <paumard@users.sourceforge.net>  Fri, 12 May 2006 19:10:20 +0200

